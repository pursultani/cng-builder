# Cloud Native GitLab: Builder Images

Cloud Native GitLab (CNG) builder images are used to build [CNG images](https://gitlab.com/gitlab-org/build/CNG).
They provide build tools, development libraries, and helper scripts that are
generally in the build stage of CNG images, hence the _CNG Builder Images_.

For the discussion on separation of the builder images from the rest of the
CNG images refer to [this issue](https://gitlab.com/gitlab-org/charts/gitlab/-/issues/4216).

## Builder Images

### Core Builder Image

This image contains common build tools and development libraries, including:

* `gcc` compiler
* `autoconf`, `make` and `cmake` tools
* `curl`, `git`, and different archive format tools
* Most of the development libraries that are used to compile GitLab artifacts
  from source.
* Helper scripts for fetching resources. `fetch` downloads and extract archives
  and has built-in support for signature verification. `gitlab-fetch` downloads
  and extracts archives from GitLab API. For usage details refer to the script
  documentation.

### Go Builder Image

This image is based on the [_Core Builder Image_](#core-builder-image) and
in addition to everything else has [Go](https://go.dev) compiler and development
tools.
