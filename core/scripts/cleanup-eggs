#!/bin/bash

#
# NAME
#
#   cleanup-eggs - removes Python packages clutter
#
# SYNOPSIS
#
#   cleanup-gems [PIP_HOME]
#
# ARGUMENTS
#
#   PIP_HOME    The path to Pip package directory. Uses `site` module to guess
#               the path when it is not specified.
#

set -euo pipefail

PIP_HOME=${1:-}

if [ -z "${PIP_HOME}" ]; then
  PIP_HOME=$(python3 -c 'import site; import os; print(os.path.dirname(site.getsitepackages()[0]))')
fi

# Exclude test directories
rm -rf \
  ${PIP_HOME}/*/test \
  ${PIP_HOME}/*/tests

# Exclude data files
rm -rf \
  ${PIP_HOME}/**/*.db \
  ${PIP_HOME}/**/*.db3 \
  ${PIP_HOME}/**/*.sqlite3 \
  ${PIP_HOME}/**/*.dbm \
  ${PIP_HOME}/**/*.dat

# Remove Python cache
find ${PIP_HOME} -name '__pycache__' -type d -exec rm -rf {} +
